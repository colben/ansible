# ansible 常用 roles

## nignx
- disable_ip.yml 注释 upstream 中的一个 ip，停止对该地址的反代
- enable_ip.yml 取消注释 upstream 中的一个 ip，开启对该地址的反代
- start_service.yml 启动 nginx 服务
- stop_service.yml 停止 nginx 服务
- restart_service.yml 重启 nginx 服务
- update_web.yml 更新一个 web 目录
- revert_web.yml 回滚一次 web 目录
- 常用操作
    - 更新 nginx 的 web 包
        ```bash
        ansible-playbook nginx.yml -e "
            host=$nginx_host
            task_name=update_zip
            app_name=$web_name
        "
        ```
    - 回滚 nginx 的 web 包
        ```bash
        ansible-playbook nginx.yml -e "
            host=$nginx_host
            task_name=revert_zip
            app_name=$web_name
        "
        ```

## tomcat
- start_service.yml 启动 tomcat 服务
- stop_service.yml 关闭 tomcat 服务
- restart_service.yml 重启 tomcat 服务
- revert_war.yml 回滚一个 war 包格式的 webapp
- update_war.yml 更新一个 war 包格式的 webapp
- revert_zip.yml 回滚一个 zip 包格式的 webapp
- update_zip.yml 更新一个 zip 包格式的 webapp
- 常用操作
    - 重启 tomcat
        ```bash
        ansible-playbook tomcat.yml -e "
            host=$tomcat_host
            task_name=restart_service
        "
        ```
    - 更新 zip 包
        ```bash
        ansible-playbook tomcat.yml -e "
            host=$tomcat_host
            task_name=update_zip
            app_name=$app_name
        "
        ```
    - 回滚 zip 包
        ```bash
        ansible-playbook tomcat.yml -e "
            host=$tomcat_host
            task_name=revert_zip
            app_name=$app_name
        "
        ```
    - 更新 war 包
        ```bash
        ansible-playbook tomcat.yml -e "
            host=$tomcat_host
            task_name=update_war
            app_name=$app_name
        "
        ```
    - 回滚 war 包
        ```bash
        ansible-playbook tomcat.yml -e "
            host=$tomcat_host
            task_name=revert_war
            app_name=$app_name
        "
        ```

## jar
- revert_jar.yml 回滚 jar 包
- update_jar.yml 更新 jar 包
- start_service.yml 启动 jar 包
- stop_service.yml 停止 jar 包
- 常用操作
    - 更新 jar 包
        ```bash
        ansible-playbook jar.yml -e "
            host=$jar_host
            task_name=update_jar
            jar_name=$jar_name
        "
        ```
    - 回滚 jar 包
        ```bash
        ansible-playbook jar.yml -e "
            host=$jar_host
            task_name=revert_jar
            jar_name=$jar_name
        "
        ```

## apk
- revert_apk.yml 回滚 apk 包
- update_apk.yml 更新 apk 包
- 重用操作
    - 更新 apk 包
        ```bash
        ansible-playbook apk.yml -e "
            host=$apk_host
            task_name=update_apk
            apk_name=$apk_name
        "
        ```
    - 回滚 apk 包
        ```bash
        ansible-playbook apk.yml -e "
            host=$apk_host
            task_name=revert_apk
            apk_name=$apk_name
        "
        ```

